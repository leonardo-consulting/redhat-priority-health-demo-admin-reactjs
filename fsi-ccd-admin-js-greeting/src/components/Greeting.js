
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getWorkList, addWorkTask } from './../actions/actions';




class Greeting extends Component {

  render() {
    return (
      <div className="ibox float-e-margins">
        <div className="ibox-title">
          <h2>Hi Chandler B. </h2>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  workLists: state.workLists,
  workTasks: state.workTasks,
});
const mapDispatchToProps = dispatch => ({
  getWorkList: () => {
    dispatch(getWorkList());
  },
  addWorkTask: (data) => {
    console.log(data);
    dispatch(addWorkTask(data));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(Greeting);
